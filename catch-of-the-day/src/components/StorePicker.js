import React,{Component,Fragment} from 'react';
import {getFunName} from '../helpers';

class StorePicker extends Component {
    myInput = React.createRef();
    goToStore = event => {
        // stop form submitting
        event.preventDefault();
        // get text from input
        console.log(this.myInput.value.value)
        const storeName = this.myInput.value.value;
        // change the page to /store/input
        this.props.history.push(`/store/${storeName}`);
    }
    render() {
        return (
            <Fragment>
            {/* Comment */}
            <form action="" className="store-selector" onSubmit={this.goToStore}>
                <h2>Please Enter A Store</h2>
                <input 
                    type="text"
                    ref={this.myInput} 
                    required 
                    placeholder="Store Name" 
                    defaultValue={getFunName()}
                />
                <button type="submit">Visit Store -></button>
            </form>
            </Fragment>
        )
    }
}

export default StorePicker;