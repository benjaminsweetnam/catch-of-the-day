import firebase from 'firebase';
import Rebase from 're-base';

const firebaseApp = firebase.initializeApp({
        apiKey: "AIzaSyC1K3rbTTbG-5gcC2VzeIY3GMFtQz7cyvs",
        authDomain: "catchoftheday-4336d.firebaseapp.com",
        databaseURL: "https://catchoftheday-4336d.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

export {firebaseApp};

export default base;